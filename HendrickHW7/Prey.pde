class Prey extends Vehicle{

    boolean isAlive;
    boolean panic;
  
    Prey(float x, float y, int radius, Vehicle target){
        super(x, y, radius, 50, 4.5, target);
        isAlive = true;
        panic = false;
    }
  
    void display(){
        strokeWeight(1);
        stroke(0);
        fill(255, 255, 0);
        ellipse(position.x, position.y, radius * 2, radius * 2);
        strokeWeight(4);
        if (panic){
            stroke(0, 0, 255);
            fill(0, 0, 255);
            arc(position.x, position.y + 3 * radius/4, radius, radius, radians(190), radians(350)); 
        }
        else{
            stroke(0, 255, 0);
            fill(0, 255, 0);
            arc(position.x, position.y, radius, radius, radians(0), radians(180)); 
        }
        ellipse(position.x - radius/2, position.y - radius/2, radius/4, radius/4);
        ellipse(position.x + radius/2, position.y - radius/2, radius/4, radius/4);
        
        stroke(255, 0, 0);
        strokeWeight(0);
        
        if (debug){
            //Velocity vector
            line(position.x, position.y, position.x + velocity.x * 10, position.y + velocity.y * 10);
        
            //Steer Vector
            stroke(0, 0, 255);
            line(position.x, position.y, position.x + steerForce.x * 5, position.y + steerForce.y * 5);
            stroke(0);
        }  
    }
    
    boolean isInPrey(PVector other, float otherRadius){
        boolean ret = false;
        float d = distance(position, other);
        if (d <= otherRadius + radius){
            ret = true; 
        }
        return ret;
    }
    
    boolean isTargetInRange(){
        boolean ret = false;
        float d = distance(position, target.position);
        if (d <= target.radius + panicRadius){
            ret = true;
        }
        panic = ret;
        return ret;
    }
    
    void calculateSteeringForces(){
        steerForce = new PVector(0,0);
        
        if (target != null && isTargetInRange()){
            steerForce.add(PVector.mult(steer.flee(target.position), targetWt)); 
        }
        
        addCommonForces(steerForce);
        
        steerForce.limit(maxForce);
        applyForce(steerForce);
    }
  
}
