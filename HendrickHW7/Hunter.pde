class Hunter extends Vehicle{

    int updateCount;
    boolean mouthOpen;
    
    Hunter(float x, float y, int radius){
        super(x, y, radius, 100, 4, null);
        updateCount = 0;
        mouthOpen = true;
        panicRadius *= 1;
    }
    
    void updateTarget(){
        float closestPrey = 2000000000;
        for (Prey p : prey){
            float d = distanceSquared(position, p.position);
            if (d < closestPrey){
                target = p;
                closestPrey = d;                
            }
        }
    }
    
    void checkForKills(){
        for (Prey p : prey){
            if (p.isInPrey(position, radius)){
                p.isAlive = false; 
            }
        }
    }
    
    void calculateSteeringForces(){
        steerForce = new PVector(0,0);
        
        if (target != null){
            steerForce.add(PVector.mult(steer.seek(target.nextPosition), targetWt)); 
        }
        
        addCommonForces(steerForce);
        
        steerForce.limit(maxForce);
        applyForce(steerForce);
    }
    
    void update(){
        updateTarget();
        
        super.update();
        checkForKills();
        if(updateCount++ == 10){
            mouthOpen = !mouthOpen;
            updateCount = 0;
        }
    }
    
    void display(){
        ellipseMode(CENTER);
        strokeWeight(1);
        fill(0, 255, 255);
               
        pushMatrix();
        translate(position.x, position.y);
        rotate(velocity.heading());
        
        stroke(0);
        if (mouthOpen){
            float startAngle = radians(40);
            float endAngle = radians(320);
            arc(0, 0, radius * 2, radius * 2, startAngle, endAngle, PIE);
        }
        else{
            ellipse(0, 0, radius * 2, radius * 2);
        }
        popMatrix();
        
        if (debug){
            //steer vector
            stroke(0, 0, 255);
            line(position.x, position.y, position.x + steerForce.x * 5, position.y + steerForce.y * 5);
            //velocity vector
            stroke(255, 0, 0);
            line(position.x, position.y, position.x + velocity.x * 10, position.y + velocity.y * 10);
        }
        stroke(0);
    }
 
}
