abstract class Vehicle{

    PVector position;
    PVector velocity;
    PVector acceleration;
    PVector friction;
    PVector forward;
    PVector right;
    PVector steerForce;
    
    PVector nextPosition;
    
    int mass;
    
    int radius;
    
    int panicRadius;
    
    float maxSpeed;
    
    float minSpeed = 0.75;
    
    int maxForce = 100;
    
    float frictionFactor = 0.01;
    
    Steer steer;
    
    Vehicle target;
    
    Vehicle(float startX, float startY, int radius, int mass, float maxSpeed, Vehicle target){
        acceleration = new PVector(0,0);
        velocity = new PVector(0,0);
        friction = new PVector(0,0);
        position = new PVector(startX, startY);
        nextPosition = position.get();
        forward = new PVector(1, 0);
        right = new PVector(0, 1);
        steerForce = new PVector(0,0);
        this.radius = radius;
        this.mass = mass; 
        this.maxSpeed = maxSpeed;
        this.steer = new Steer(this);
        this.target = target;
        this.panicRadius = radius * 4;
    }
    
    void applyForce(PVector force){
        PVector accel = PVector.div(force, mass);
        acceleration.add(accel); 
    }
    
    abstract void calculateSteeringForces();
    
    int getUpwardDirection(){
        int ret = UP;
        if ((forward.heading() > 0) && (forward.heading() < radians(180))){
            ret = DOWN;
        }  
        return ret;
    }
    
    int getSidewaysDirection(){
        int ret = LEFT;
        if ((forward.heading() > radians(-90)) && (forward.heading() < radians(90))){
            ret = RIGHT;
        }  
        return ret; 
    }
    
    void addCommonForces(PVector force){
        int nearWall = isNearWall(position, radius);
        
        if (nearWall == DOWN){
            if (getSidewaysDirection() == LEFT){
                force.add(PVector.mult(steer.seek(new PVector(0, height/2)), wallWt));
            }
            else{
                force.add(PVector.mult(steer.seek(new PVector(width, height/2)), wallWt));
            } 
        }
        
        else if (nearWall == LEFT){
            if (getUpwardDirection() == UP){
                force.add(PVector.mult(steer.seek(new PVector(width/2, 0)), wallWt));
            }
            else{
                force.add(PVector.mult(steer.seek(new PVector(width/2, height)), wallWt));
            }
        }
        else if (nearWall == RIGHT){
            if ((getUpwardDirection() == UP) || position.y > height - radius * 2){
                force.add(PVector.mult(steer.seek(new PVector(width/2, 0)), wallWt));
            }
            else{
                force.add(PVector.mult(steer.seek(new PVector(width/2, height * 2)), wallWt)); 
            } 
        }
        else if (nearWall == UP){
            if (getSidewaysDirection() == LEFT){
                force.add(PVector.mult(steer.seek(new PVector(0, height/2)), wallWt));
            }
            else{
                force.add(PVector.mult(steer.seek(new PVector(width, height/2)), wallWt));
            } 
        }
        
        for (Obstacle o : obstacles){
            force.add(PVector.mult(steer.avoidObstacle(o, panicRadius), obstacleWt)); 
        }
    }
    
    void applyFriction(){
         friction.x = -velocity.x * frictionFactor;
         friction.y = -velocity.y * frictionFactor;
         velocity.add(friction);
    }
    
    void update(){
        calculateSteeringForces();
        velocity.add(acceleration);
        applyFriction();
        velocity.limit(maxSpeed);
        if (velocity.mag() == 0){
            velocity = new PVector(random(-maxSpeed/2, maxSpeed/2), random(-maxSpeed/2, maxSpeed/2));
        }
        else if (velocity.mag() < minSpeed){
            velocity.setMag(minSpeed); 
        }
        position = nextPosition.get();
        nextPosition.add(velocity);
        forward = velocity.get();
        forward.normalize();
        right = new PVector(-forward.y, forward.x);
        acceleration.mult(0);
    }
    
    abstract void display();

}
