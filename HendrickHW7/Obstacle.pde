class Obstacle{
    
    int radius;
    PVector position;
    color col;
  
    Obstacle(float x, float y, int radius){
        position = new PVector(x, y);
        this.radius = radius;
        this.col = color(random(0, 255), random(0, 255), random(0, 255));
    }
    
    void display(){
        fill(col);
        ellipse(position.x, position.y, radius * 2, radius * 2); 
    }
}

