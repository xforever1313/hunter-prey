float distance(PVector v1, PVector v2){
    return sqrt(distanceSquared(v1, v2)); 
}

float distanceSquared(PVector v1, PVector v2){
    return (abs(v1.x - v2.x) * abs(v1.x - v2.x)) + (abs(v1.y - v2.y) * abs(v1.y - v2.y));
}

int isNearWall(PVector position, float radius){
    int ret = -1;
    if (position.x - radius * 2 < 0){
        ret = LEFT;
    }
    else if (position.x + radius * 2 > width){
        ret = RIGHT; 
    }
    else if (position.y - radius * 2 < 0){
        ret = UP; 
    }
    else if (position.y + radius * 2 > height){
        ret = DOWN; 
    }
    return ret;
}
