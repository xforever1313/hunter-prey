Hunter hunter;

int numberOfPrey = 15;
int numberOfObstacles = 6;
int prayRadius = 10;
ArrayList<Prey> prey;
ArrayList<Obstacle> obstacles;
int targetWt = 5;
int wallWt = 10;
int obstacleWt = 7;

boolean debug = true;

void setup(){
    size(600,600);
    hunter = new Hunter(width/2, height/2, 20);
    prey = new ArrayList<Prey>();
    for(int i = 0; i < numberOfPrey; ++i){
        prey.add(new Prey(random(prayRadius, width - prayRadius), random(prayRadius, height - prayRadius), prayRadius, hunter));
    }
    hunter.updateTarget();
    
    obstacles = new ArrayList<Obstacle>();
    int maxRadius = 30;
    for (int i = 0; i < numberOfObstacles; ++i){
        int radius = round(random(10, maxRadius));
        float x = random(radius + hunter.radius * 3, width - radius - hunter.radius * 3);
        float y = random(radius + hunter.radius * 3, height - radius - hunter.radius * 3);
        boolean add = true;
        for (int j = 0; (j < obstacles.size()) && add; ++j){
            PVector location = new PVector(x, y);
            if (location.dist(obstacles.get(j).position) < ((radius + hunter.radius) * 2 + obstacles.get(j).radius)){
                add = false;  //Do not add if close to another obstacle.
            } 
        }
        if (add){
            obstacles.add(new Obstacle(x, y, radius));
        }
        else{
            --i; 
        }
    }
}

void updatePrey(){
    for (int i = 0; i < prey.size(); ++i){
        if (!prey.get(i).isAlive){
            prey.set(i, new Prey(random(prayRadius, width - prayRadius), random(prayRadius, height - prayRadius), prayRadius, hunter));
        }
        prey.get(i).update();
    } 
}

void drawObstacles(){
    for (Obstacle o : obstacles){
        o.display();
    } 
}

void drawPrey(){
    for (Prey p : prey){
        p.display();
    } 
}

void draw(){
    background(210);
    updatePrey();
    hunter.update();
    drawObstacles();
    drawPrey();
    hunter.display();
}

void keyPressed(){
    if(key == 'd' || key == 'D'){
        debug = !debug;
    } 
}
