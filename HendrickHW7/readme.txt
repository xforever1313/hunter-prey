Hunter-Prey
Seth Hendrick

Instructions:
D - turn on/off debug lines.
Sit back and enjoy!

-------
The hunter is the blue circle that opens and closes his mouth.

The prey are the smaller yellow circles with faces.  The faces are smiley green faces when not threatened, and frowny blue faces when threatened.  When a prey is eaten, another re-spawns in a random position on the stage.

All other circles are obstacles.

When debug vectors are turned on, the red line is the velocity and the blue line is the steer force.  Both vectors are scaled differently to make them visible.  Velocity is scaled by 10, and steer is scaled by 5.

The Prey travel slightly faster than the hunter, and have less mass.  But, they only run from the hunter's current position.
The hunter travels slower, and has more mass, but is able to predict the prey's next position, and targets that.
